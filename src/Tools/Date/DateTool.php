<?php
namespace Tools\Date;

/**
 * 日期处理工具类
 * Class DateTool
 */
class DateTool
{

    /**
     * 返回当前的毫秒时间戳
     * @param bool $tinyTime 微秒时间true | false
     * @return string
     */
    public static function msecsTime(bool $tinyTime = false): string
    {
        list($msecs, $sec) = explode(' ', microtime());
        $ratio = $tinyTime ?  1000000 : 1000;
        return sprintf('%.0f', (floatval($msecs) + floatval($sec)) * $ratio);
    }
}